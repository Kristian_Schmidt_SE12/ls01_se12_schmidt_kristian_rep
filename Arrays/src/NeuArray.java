import java.util.Scanner;
import java.util.Random;

public class NeuArray {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		
//1. Erzeugen Sie ein Array namens myArray, das 10 Werte speichern kann.
		int [] intArray = new int[10];
		
//2. Speichern Sie 10 beliebige Werte in Ihrem Array.
//3. Lassen Sie mit Hilfe einer for-Schleife die 10 Werte ausgeben.
		Random rd = new Random();
	    for (int i = 0; i < 10; i++) {
	    	intArray[i] = rd.nextInt();
	    	System.out.println("Index" + i + ": " + intArray[i]);
	    }
	    System.out.println("");

//4. Speichern Sie am Index 3 den Wert 1000.
	    intArray[3] = 1000;
	    
//5. Lassen Sie zum �berpr�fen den Wert des Index 3 ausgeben.
	    for(int index = 0; index < 10; index++)
		{ System.out.println("Index" + index + ": " + intArray[index]);
		}
	    System.out.println("");
	    
//6. �berschrieben Sie alle Werte mit Hilfe einer for-Schleife, so dass nur noch 0-en in Ihrem Array stehen.
	    for(int index = 0; index < 10; index++)
	    intArray[index] = 0;
	    for(int index = 0; index < 10; index++)
		{ System.out.println("Index" + index + ": " + intArray[index]);
		}
	    
//7. Erstellen Sie nun ein Men� mit folgenden Punkten:
//a. Alle Werte ausgeben
//b. Einen bestimmten Wert ausgeben
//c. Das Array komplett mit neuen Werten bef�llen
//d. Einen bestimmten Wert �ndern
//8. Erweitern Sie Ihr Programm von 7. so, dass der Benutzer entscheiden kann, ob es wiederholt werden soll oder nicht.
	}
}